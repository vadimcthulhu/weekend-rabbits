import Vue from 'vue'
import Vuex from 'vuex'
import rabbits from '@/store/rabbits'
import user from '@/store/user'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {
        rabbits: rabbits,
        user: user,
    }
})
