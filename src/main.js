import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'

axios.interceptors.response.use((response) => {
    if (response.status === 401) {
        store.commit({
            type: 'user/logout',
        });
    }
    return response;
}, function (error) {
    return Promise.reject(error.response);
});

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');

router.beforeEach((to, from, next) => {
    if (to.meta.requiresAuth) {
        if (store.getters['user/isLogin']) {
            next();
        } else {
            next('/');
        }
    } else {
        next();
    }
});