import Vue from 'vue';
import router from '@/router';
import axios from 'axios';
import qs from 'qs';
import { API_BASE } from '@/data';

export default {
    namespaced: true,
    state: {
        list: {}
    },
    getters: {
        list: state => {
            return state.list;
        },

    },
    mutations: {
        setList(state, payload){
            state.list = payload.data;
        },
        deleteRabbit(state, payload) {
            const index = state.list.map(rabbit => rabbit.id).indexOf(payload.id);
            Vue.delete(state.list, index);
        },
        editRabbit(state, payload) {
            const index = state.list.map(rabbit => rabbit.id).indexOf(payload.item.id);
            Vue.set(state.list, index, payload.item);
            router.push({ name: 'home'});
        },
        addRabbit(state, payload) {
            Vue.set(state.list, state.list.length, payload.item);
            router.push({ name: 'home'});
        }
    },
    actions: {
        setList({commit}) {
            axios({
                method: 'GET',
                url: API_BASE + 'rabbit/list',
            }).then((response) => {
                commit({
                    type: 'setList',
                    data: response.data
                });
            });
        },
        addRabbit({commit, state}, payload) {
            axios({
                method: 'POST',
                url: API_BASE + 'rabbit',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: qs.stringify({
                    rabbit: {
                        name: payload.item.name,
                        weight: payload.item.weight,
                    }
                })
            }).then(() => {
                commit({
                    type: 'addRabbit',
                    item: {
                        ...payload.item,
                        id: state.list[state.list.length - 1].id + 1
                    }
                });
            })
        },
        editRabbit({commit}, payload) {
            axios({
                method: 'POST',
                url: API_BASE + 'rabbit/' + payload.item.id,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: qs.stringify({
                    rabbit: {
                        name: payload.item.name,
                        weight: payload.item.weight,
                    }
                })
            }).then(() => {
                commit({
                    type: 'editRabbit',
                    item: payload.item
                });
            })
        },
        deleteRabbit({commit}, payload){
            axios({
                method: 'DELETE',
                url: API_BASE + 'rabbit/' + payload.id,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            }).then(() => {
                commit({
                    type: 'deleteRabbit',
                    id: payload.id
                });
            })
        }
    }
}