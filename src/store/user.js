import axios from 'axios';
import { API_BASE } from '@/data';

export default {
    namespaced: true,
    state: {
        isLogin: false,
        details: localStorage.token ? JSON.parse(atob(localStorage.token.split('.')[1])) : {}
    },
    getters: {
        isLogin: state => {
            return state.isLogin;
        },
        details: state => {
            return state.details;
        }
    },
    mutations: {
        login(state, payload) {
            state.token = payload.token;
            state.isLogin = true;
            state.details = payload.details;
            axios.defaults.headers.common['Authorization'] = payload.token;
            localStorage.token = payload.token;
            this.dispatch({
                type: 'rabbits/setList',
            });
        },
        isLogin(state, payload) {
            axios.defaults.headers.common['Authorization'] = payload.token;
            state.details = JSON.parse(atob(payload.token.split('.')[1]));
            state.isLogin = payload.isLogin;
            this.dispatch({
                type: 'rabbits/setList',
            });
        },
        logout(state) {
            state.isLogin = false;
            localStorage.token = '';
        }
    },
    actions: {
        login({commit}, payload) {
            return axios({
                method: 'POST',
                url: API_BASE + 'login_check',
                headers: { 'Content-Type': 'application/json' },
                data: {
                    username: payload.username,
                    password: payload.password,
                }
            }).then((response) => {
                commit({
                    type: 'login',
                    details: JSON.parse(atob(response.data.token.split('.')[1])),
                    token: 'Bearer' + ' ' + response.data.token
                });
            });
        },
    }
}